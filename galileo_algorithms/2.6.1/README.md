Linear Search
=============
An example of linear search that returns the first position in a list (array of size n) L[0:n-1] if the search element X occusrs or return -1 if X is not in the list.

File(s)
-------
* Sequential linear search (linear.c)
* Linear search Arduino (s_linear.ino)

Reference(s)
------------
* "Algorithm: sequential, parallel and distributed" by Kenneth A. Berman and Jerome L. Paul, Chapter 2: Design and Analysis Fundamentals, section 2.6.1, page 34
