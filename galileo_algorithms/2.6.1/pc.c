/*
 * Linear Search 
 * based on ALgorithm: Sequential, Parallel and Distributed book (page 34)
 * 
 * Description: The program implement a linear search that returns the first position 
 * in a list of array of size N. If the search element X occurs or returns -1 if X is 
 * not in the list.
 */
#include <stdio.h> // printf
#include <stdlib.h> // srand, rand
#include <time.h> // time

#define N 100

void generate(int*);
int linearsearch(int*, int);

int main(int argc, char* argv[]) {
	
	if(argc < 2) { fprintf(stderr, "usage: e.g: $ ./a.out <value-between-1-to-100>\n\n"); exit(1); }

	const int value = atoi(argv[1]); // get the value from the parameters

	int *list; list = (int*) calloc(N, sizeof(int)); // mem alloc the list
	generate(list); // generate random seeds for the list
	printf("%d found with index = %d\n", value, linearsearch(list, value)); // find the X in the list

	return 0;
}

void generate(int* array) {
	srand(time(NULL));
	int i; for(i = 0; i < N; i++) array[i] = rand() % 100;
}

int linearsearch(int* list, int X) {
	int i; for(i = 0; i < N; i++) {
		if(X == list[i]) return i;
	}

	return (-1);
}
