/*
 * Linear Search
 * 
 * hadrihl hadrihilmi@gmail.com
 */

// var init
const int N = 100;
const int x = 71;
int *list; 

// generate random number, two seeds
long int random(long int minimum, long int maximum) {
	return (random() + minimum) % maximum;
}

// generate random number, one seed
long int random(long int maximum) {
	return random() % maximum;
}

void setup() {
	Serial.begin(9600); // init serial baud
        list = (int*) calloc(N, sizeof(int));

	// init array list
	int i; for(i = 0; i < N; i++) {
		list[i] = random(N); 
		Serial.print("list["); Serial.print(i); Serial.print("] = "); 
		Serial.println(list[i]);
	}
}

void loop() {
  int i; for(i = 0; i < N; i++) {
    if(x == list[i]) Serial.println("Found");
    else Serial.println("Not found.");
  }
}
