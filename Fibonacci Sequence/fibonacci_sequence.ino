int ledr = 8;
int ledg = 3;

void setup()
{
  pinMode(ledr, OUTPUT);
  pinMode(ledg, OUTPUT);
  Serial.begin(9600); 
}

void loop()
{
  int n = 47;
  digitalWrite(ledr,LOW);
  digitalWrite(ledg, LOW);
  delay(2000);
  if(n==1||n==2)
  {
    digitalWrite(ledr,LOW);
    digitalWrite(ledg,LOW);
    delay(2000);
  }
  if(n>=3)
  {
    unsigned long fib[n];
    fib[0] = 0;  // changed from 1
    fib[1] = 1;
    for(int i = 2; i<n; i++)
    {
      fib[i]=fib[i-2]+fib[i-1];
      Serial.println(fib[i]);

    }
    digitalWrite(ledr,LOW);
    digitalWrite(ledg,HIGH);
    delay(5000);
  }
  digitalWrite(ledr,HIGH);
  digitalWrite(ledg,LOW);
  delay(2000);
}
