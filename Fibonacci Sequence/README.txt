Fibonnaci Sequence
=============================
An example of fibonnaci sequence outcome with intel galileo

File(s)
-------
*  Fibonacci Sequence (fibonacci_sequence.ino)

Reference(s)
------------
http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibBio.html
http://www.mathsisfun.com/numbers/fibonacci-sequence.html
http://en.wikipedia.org/wiki/Fibonacci_number


by : Khairol Nadzrin Saufi
