Web Server LED control part 1
=============================
An example of web server to control (turn on and off) the LED light by using intel galileo ethernet shield with personal computer ethernet port.

File(s)
-------
* Web server LED Control (web_server_LED_control.ino)

Reference(s)
------------
http://startingelectronics.com
http://arduino.cc/en/ArduinoCertified/IntelGalileo

By : Khairol Nadzrin Saufi