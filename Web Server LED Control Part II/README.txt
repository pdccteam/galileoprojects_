Web Server LED control part II
=============================
An example of web server to control (turn on and off) the LED light by using intel galileo ethernet shield with local area network.

File(s)
-------
* Web server LED Control (web_server_LED_control.ino)
* Change static ip (static_ip.ino)
* DHCP lookup (dhcp_look.ino)

Reference(s)
------------
http://startingelectronics.com
http://arduino.cc/en/ArduinoCertified/IntelGalileo
https://drive.google.com/file/d/0B3ezX1cozXh4Y2dTRE5DMUpRSzg/edit?usp=sharing


By : Khairol Nadzrin Saufi

